package com.tsystems.javaschool.tasks.calculator;

public interface Calculable {
    public String evaluate(String statement);
}
