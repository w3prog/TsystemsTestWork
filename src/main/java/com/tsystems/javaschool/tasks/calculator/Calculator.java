package com.tsystems.javaschool.tasks.calculator;

public class Calculator implements Calculable {
    public String evaluate(String statement){
        Calculable calc = new StackCalculator();
        return calc.evaluate(statement);
    }
}
