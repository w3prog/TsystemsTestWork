package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.Locale;
import java.util.Stack;

public class StackCalculator implements Calculable {

    private final Stack<Double> values = new Stack<Double>();
    private final Stack<Character> ops = new Stack<Character>();

    @Override
    public String evaluate(String statement) {

        if (statement == null){
            return null;
        }
        try {
            char[] tokens = statement.toCharArray();
            for (int i = 0; i < tokens.length; i++) {
                if (Character.isDigit(tokens[i])) {
                    StringBuffer sbuf = new StringBuffer();
                    while(!checkOperand(tokens[i])){
                        sbuf.append(tokens[i]);
                        i++;
                        if(i == tokens.length)
                            break;
                    }
                    i--;
                    values.push(Double.parseDouble(sbuf.toString()));
                }

                else if (tokens[i] == '(')
                    ops.push(tokens[i]);

                else if (tokens[i] == ')') {
                    while (ops.peek() != '(')
                        values.push(applyOperation(ops.pop(), values.pop(), values.pop()));
                    ops.pop();
                }
                else if (tokens[i] == '+' || tokens[i] == '-' ||
                        tokens[i] == '*' || tokens[i] == '/') {

                    while (!ops.empty() && hasHighPriority(tokens[i], ops.peek()))
                        values.push(applyOperation(ops.pop(), values.pop(), values.pop()));

                    ops.push(tokens[i]);
                }
            }

            while (!ops.empty()){
                values.push(applyOperation(ops.pop(), values.pop(), values.pop()));
            }

            return formatResult(values.pop());
        }
        catch (ParserException | NumberFormatException |EmptyStackException exp){
            return null;
        }

    }

    private boolean checkOperand(char charAt) {
        if(charAt == ',') throw new ParserException();
        return ("+-/*%^=()".indexOf(charAt)) != -1;
    }

    public static boolean hasHighPriority(char op1, char op2) {
        if (op2 == '(' || op2 == ')')
            return false;
        if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
            return false;
        else
            return true;
    }

    private static double applyOperation(char op, double b, double a) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0)
                    throw new ParserException();
                return a / b;
        }
        return 0;
    }

    String formatResult(double result){

        String str = String.format(Locale.US,"%.4f", result);
        int zeros = 0;
        while (str.charAt(str.length() - 1 -  zeros) == '0'){
            zeros++;
        }
        if(zeros>0){
            str = str.substring(0,str.length() - zeros);
        }

        if (str.endsWith(".")){
            str = String.format("%s", (int) result);
        }
        return (str);
    }
}
