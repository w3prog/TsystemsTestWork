package com.tsystems.javaschool.tasks.calculator;


import java.util.Locale;


public class ExecCalculator implements Calculable {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    final int NONE = 0;         
    final int OPERAND = 1;
    final int NUMBER = 2;       
    final String EXPRESSION_END = "\0";

    private String expression;
    private int expIndex;
    private String token;
    private int tokenType;


    public String evaluate(String statement){
        if (statement == null || statement.equals("")){
            return null;
        }

        try {
            double result;

            expression = statement;
            expIndex = 0;
            getToken();

            result = evalLowPriority();

            if(!token.equals(EXPRESSION_END))
                throw new ParserException();

            return formatResult(result);
        }
        catch (ParserException exp){
            return null;
        }

    }


    private void getToken(){
        tokenType = NONE;
        token = "";

        if(expIndex == expression.length()){
            token = EXPRESSION_END;
            return;
        }

        if(checkOperand(expression.charAt(expIndex))){
            token += expression.charAt(expIndex);
            expIndex++;
            tokenType = OPERAND;
        }
        else if (Character.isDigit(expression.charAt(expIndex))){
            while(!checkOperand(expression.charAt(expIndex))){
                token += expression.charAt(expIndex);
                expIndex++;
                if(expIndex >= expression.length())
                    break;
            }
            tokenType = NUMBER;
        }
        else {
            throw new ParserException();
        }
    }

    private boolean checkOperand(char charAt) {
        return ("+-/*%^=()".indexOf(charAt)) != -1;
    }

    private double evalLowPriority() throws ParserException{

        char op;
        double result;
        double partialResult;
        result = evalHighPriority();
        while((op = token.charAt(0)) == '+' || op == '-'){
            getToken();
            partialResult = evalHighPriority();
            switch(op){
                case '+':
                    result += partialResult;
                    break;
                case '-':
                    result -= partialResult;
                    break;
            }
        }
        return result;
    }

    private double evalHighPriority() throws ParserException{

        char op;
        double result;
        double partialResult;

        result = calcBrackets();
        while((op = token.charAt(0)) == '*' || op == '/' ){
            getToken();
            partialResult = calcBrackets();
            switch(op){
                case '*':
                    result *= partialResult;
                    break;
                case '/':
                    if(partialResult == 0.0)
                        throw new ParserException();
                    result /= partialResult;
                    break;
            }
        }
        return result;
    }

    private double calcBrackets() throws ParserException{
        double result;

        if(token.equals("(")){
            getToken();
            result = evalLowPriority();
            if(!token.equals(")"))
                throw new ParserException();
            getToken();
        }
        else
            result = readNumber();
        return result;
    }

    private double readNumber() throws ParserException{

        double result = 0.0;
        switch(tokenType){
            case NUMBER:
                try{
                    result = Double.parseDouble(token);
                }
                catch(NumberFormatException exc){
                    throw new ParserException();
                }
                getToken();
                break;
            default:
                throw new ParserException();
        }
        return result;
    }

    String formatResult(double result){
        String str = String.format(Locale.US,"%.4f", result);
        int zeros = 0;
        while (str.charAt(str.length() - 1 -  zeros) == '0'){
            zeros++;
        }
        if(zeros>0){
            str = str.substring(0,str.length() - zeros);
        }

        if (str.endsWith(".")){
            str = String.format("%s", (int) result);
        }
        return (str);
    }

}
