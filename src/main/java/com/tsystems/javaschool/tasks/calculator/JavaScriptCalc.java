package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class JavaScriptCalc implements Calculable {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (statement == null){
            return null;
        }
        try{
            checkStatement(statement);
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            String result = engine.eval(statement).toString();

            if (result.equals("Infinity")){
                throw new ParserException();
            }

            return result;
        }catch (Exception e){
            return null;
        }
    }

    public boolean checkStatement(String statement) throws ParserException{
        if (statement.contains(",")) throw new ParserException();
        if (statement.contains("//")) throw new ParserException();
        return true;
    }

}
