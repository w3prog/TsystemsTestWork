package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.tsystems.javaschool.tasks.pyramid.CannotBuildPyramidException;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers){

        int n = 0;
        int size = 0;

        while (n < inputNumbers.size()){
            size++;
            n += size;
        }

        for (int i = 0; i < inputNumbers.size(); i++) {
            if (inputNumbers.get(i) == null){
                throw new CannotBuildPyramidException();
            }
        }

        if( n != inputNumbers.size() || inputNumbers.size() > 10000000 ){
            throw new CannotBuildPyramidException();
        }


        Collections.sort(inputNumbers);

        int[][] answer = new int[size][size*2-1];

        if (size == 1){
            return new int[][]{{inputNumbers.get(0)}};
        }


        int index = 0;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < i + 1; j++) {
                int value = inputNumbers.get(index);
                answer[i][ size - 1 - i + j * 2 ] = value;
                index++;
            }
        }
        return answer;
    }


}
